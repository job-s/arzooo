import React, { useEffect } from "react";
import { connect } from "react-redux";

import Header from "src/common/components/header/header";
import ProductDescription from "./common/components/productDescription/productDescription";
import ProductImages from "./common/components/productImages/productImages";

import { getLoading, doSomething } from "./ducks";

import "./home.scss";

const Home = ({ loading, doSomething }) => {
	useEffect(() => {
		setTimeout(() => {
			doSomething();
		}, 1000);
	}, []);

	return (
		<div className="home-page">
			<Header />
			<div className="home-page-container">
				<ProductImages />
				<ProductDescription />
			</div>
		</div>
	);
};

const enhance = connect(
	(state) => ({
		loading: getLoading(state),
	}),
	{
		doSomething,
	}
);

export default enhance(Home);
