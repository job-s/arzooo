import React from "react";

import ImageCarousel from "../../../../../common/components/imageCarousel/imageCarousel";
import ProductImage from "../productImage/productImage";
import BuyOptions from "../buyOptions/buyOptions";

import "./productImages.scss";

const ProductImages = () => {
	return (
		<div className="product-images">
			<ProductImage />
			<ImageCarousel />
			<BuyOptions />
		</div>
	);
};

export default ProductImages;
