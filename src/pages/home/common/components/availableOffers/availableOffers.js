import React from "react";

import "./availableOffers.scss";

const AvailableOffers = () => {
	return (
		<div className="available-offers">
			<p>Available Offers</p>
			<ul>
				<li>Bank offer 10% Instant Discount with HDFC Bank Credit</li>
				<li>Cards and Credit/Debit EMI Transactions T&C</li>
				<li>Bank Offer 10% Instant Discount with HDFC bank Debit</li>
				<li>Card Transactions T&C</li>
			</ul>
			<a>View 4 more offers</a>
		</div>
	);
};

export default AvailableOffers;
