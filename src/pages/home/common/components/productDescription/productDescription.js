import React from "react";
import StarRatings from "react-star-ratings";

import AvailableOffers from "../availableOffers/availableOffers";
import Delivery from "../delivery/delivery";

import "./productDescription.scss";

const ProductDescription = () => {
	return (
		<div className="product-description">
			<div className="p-d-product-name">
				<span>LG R202F 80cm (32cm) HD Ready LED TV</span>
				<span>(KLV-32R202F)</span>
			</div>
			<div className="p-d-product-price-rating">
				<div>
					<span>$89,000</span>
					<span>$98,000</span>
					<span>35%</span>
				</div>
				<div>
					<StarRatings rating={2} starDimension={"25px"} starSpacing={"3px"} />
					<span>400 Reviews</span>
				</div>
			</div>
			<AvailableOffers />
			<p className="p-d-product-warranty">1 year manufacture warranty</p>
			<Delivery />
			<div className="p-d-product-highlights">
				<div className="p-d-product-highlights-ch">
					<p>Highlights</p>
					<div>
						<p>Resolution: HD Ready 1366 x 768 pixels</p>
						<p>Sound Output: 20W</p>
						<p>Refresh Rate: 50Hz</p>
					</div>
				</div>
				<div className="p-d-product-highlights-ch">
					<p>Seller</p>
					<div>
						<p>White Orange, HSR</p>
						<p>View more sellers</p>
					</div>
				</div>
			</div>
		</div>
	);
};

export default ProductDescription;
