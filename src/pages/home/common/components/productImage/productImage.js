import React from "react";

import "./productImage.scss";

const ProductImage = () => {
	return (
		<div className="product-image">
			<img
				src="https://images.freeimages.com/images/large-previews/647/snowy-mountain-1378865.jpg"
				alt="product image"
			/>
		</div>
	);
};

export default ProductImage;
