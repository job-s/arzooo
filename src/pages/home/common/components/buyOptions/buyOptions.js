import React from "react";

import "./buyOptions.scss";

const BuyOptions = () => {
	return (
		<div className="buy-options">
			<button className="button button-add-to-cart">ADD TO CART</button>
			<button className="button button-buy">BUY</button>
		</div>
	);
};

export default BuyOptions;
