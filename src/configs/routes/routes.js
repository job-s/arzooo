import React, { Suspense } from "react";
import { Router, Switch, Redirect } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";

import { customHistory } from "../config";
import { PrivateRoute } from "../../common/components/routes/PrivateRoute";
import { PublicRoute } from "../../common/components/routes/PublicRoute";

import Home from "../../pages/home/home";

const Routes = () => {
	return (
		<Suspense fallback={<div>Loading...</div>}>
			<ConnectedRouter history={customHistory}>
				<Switch>
					<PublicRoute path="/home" component={Home} redirectPath="/home" />
					<Redirect to="/home" />
				</Switch>
			</ConnectedRouter>
		</Suspense>
	);
};

export default Routes;
